<?php

 class Download extends CI_Controller{

 	public function index () 
	{
 		$this->load->helper('download');


 		$data = file_get_contents(base_url().'assets/faisal-punnakkadan.pdf');
 		$name = "faisal-punnakkadan.pdf";

 		force_download($name, $data);
 		
 	}
 }