<?php
class Index extends CI_Controller {
	 
        protected $header = 'templates/header';
        protected $footer = 'templates/footer';

        public function __construct()
        {
            parent::__construct();

            $this->load->helper('form');
            $this->load->library('email');
            $this->load->helper('text');
            $this->load->model('Testimonial_model', 'testimonial');
            $this->load->model('Media_model', 'media');
            $this->load->model('News_model', 'news');
        }

        protected $current = '';

        public function index()
        {
            $data['testimonials'] = $this->testimonial->get_all();
            $data['newss'] = $this->news->limit(3)->order_by('id','desc')->get_all();
            
            $this->current = 'index';
            $this->load->view($this->header, ['current' => $this->current]);
            $this->load->view('index',$data);
            $this->load->view($this->footer);

            if ($this->input->post()) {
                $this->form_validation->set_rules('email', 'Email', 'required');
                if ($this->form_validation->run() === FALSE)
                {
                    redirect('/');
                }else {

                    $config = [
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://md-in-15.webhostbox.net',            
                        'smtp_port' => 465,            
                        'smtp_user' => 'no-reply@nimswandoor.com',            
                        'smtp_pass' => 'ceMo)nYS3p@k',    
                        'mailtype' => 'html',
                        'charset' => 'iso-8859-1',
                        'wordwrap' => TRUE,
                        'smtp_crypto'=>'ssl'
                    ];
                    $this->email->initialize($config);

                    $name = $this->input->post('name');
                    $email = $this->input->post('email');
                    $message = $this->input->post('message');
                    
                    $content  = 'Name    :   ' . $name . "\r\n";
                    $content .= 'Email   : ' . $email . "\r\n" ;
                    $content .= 'Message   :  ' . $message . "\r\n";
                    
                    $this->email->set_newline("\r\n");
                    $this->email->from('mail@palsresidency.com');
                    $this->email->to('palsresidency@gmail.com');
                    $this->email->subject('Message from web');

                    $this->email->message($content);

                    if ($this->email->send()) {
                        echo "<script> alert('Thanks for getting in touch. We’re on it.');
                        </script>";
                    }
                    else {
                        echo "<script> alert('Try again later');
                        </script>";
                    }
                }
            }
        }

        public function gallery()
        {
            $data['medias'] = $this->media->get_all();
            $this->current = 'gallery';
            $this->load->view($this->header, ['current' => $this->current]);
            
            $this->load->view('gallery',$data);
            $this->load->view($this->footer);
        }

        public function blog_listing()
        {
            // $data['newss'] = $this->news->get_all();
            $total_post = $this->news->count_rows();
            $data['newss'] = $this->news->order_by('id', 'desc')->paginate(12, $total_post);
            $data['all_pages'] = $this->news->all_pages;
            // $data['previous_page'] = $this->news->previous_page; 
            // $data['next_page'] = $this->news->next_page;

            $this->current = 'blog-listing';

            $this->load->view($this->header, ['current' => $this->current]);
            $this->load->view('blog-listing',$data);
            $this->load->view($this->footer);
        }

        public function blog_single($id)
        {
            $data['newss'] = $this->news->where('id',$id)->order_by('id','desc')->get_all();
            $data['news1'] = $this->news->limit(5)->order_by('id','desc')->get_all();
            $this->current = 'blog-single';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('blog-single',$data);
            $this->load->view($this->footer);
        }

        public function suggestions()
        {
            $this->current = 'suggestions';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('suggestions');
            $this->load->view($this->footer);

            if ($this->input->post()) {
                $this->form_validation->set_rules('name', 'Name', 'required');
                if ($this->form_validation->run() === FALSE)
                {
                    redirect('/');
                }else {

                    $config = [
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://md-in-15.webhostbox.net',            
                        'smtp_port' => 465,            
                        'smtp_user' => 'no-reply@nimswandoor.com',            
                        'smtp_pass' => 'ceMo)nYS3p@k',    
                        'mailtype' => 'html',
                        'charset' => 'iso-8859-1',
                        'wordwrap' => TRUE,
                        'smtp_crypto'=>'ssl'
                    ];
                    $this->email->initialize($config);

                    $name = $this->input->post('name');
                    $number = $this->input->post('number');
                    $message = $this->input->post('message');
                    
                    $content  = 'Name    :   ' . $name . "\r\n";
                    $content .= 'Contact No.   : ' . $number . "\r\n" ;
                    $content .= 'Message   :  ' . $message . "\r\n";
                    
                    $this->email->set_newline("\r\n");
                    $this->email->from('no-reply@nimswandoor.com', 'Nims');        
                    $this->email->to('nimswdr@gmail.com');
                    $this->email->subject('Suggestion from web');

                    $this->email->message($content);

                    if ($this->email->send()) {
                        echo "<script> alert('Thanks For Your Suggestion . We’re On It.');
                        </script>";
                    }
                    else {
                        echo "<script> alert('Try again later');
                        </script>";
                    }
                }
            }
        }

}