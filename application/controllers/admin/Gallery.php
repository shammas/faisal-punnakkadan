<?php
/**
 * Gallery_Controller.php
 * Date: 11/01/19
 * Time: 10:13 AM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller
{

    //        public $delete_cache_on_save = TRUE;
    function __construct()
    {
        parent::__construct();
        $this->load->model('Gallery_model', 'gallery');
        $this->load->model('Gallery_file_model', 'gallery_file');
        $this->load->library(['upload', 'image_lib', 'ion_auth', 'form_validation']);
        $this->load->helper('url');

        if (!$this->ion_auth->logged_in()) {
            redirect(base_url('login'));
        }
    }

    function index()
    {
        $data = $this->gallery->with_files()->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function get_all()
    {
        $data = $this->gallery->with_files()->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function store()
    {
       $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);

            unset($post_data['uploaded']);

            $gallery_id = $this->gallery->insert($post_data);
            if ($gallery_id) {
                if (!empty($uploaded) ) {

                    foreach ($uploaded as $upload) {

                        /*INSERT FILE DATA TO DB*/
                        $file_data['gallery_id'] = $gallery_id;
                        $file_data['file_name'] = $upload->file_name;
                        $file_data['url'] = base_url() . 'uploads/gallery/';
                        $file_data['path'] = getwdir() . 'uploads/gallery/';

                        $file_id = $this->gallery_file->insert($file_data);

                        if ($file_id) {
                            if (!is_dir(getwdir() . 'uploads/gallery/thumb')) {
                                mkdir(getwdir() . 'uploads/gallery/thumb', 0777, TRUE);
                            }

                            /*****Create Thumb Image****/
                            $img_cfg['source_image'] = getwdir() . '/uploads/gallery/' . $upload->file_name;
                            $img_cfg['maintain_ratio'] = TRUE;
                            $img_cfg['new_image'] = getwdir() . 'uploads/gallery/thumb/' . $upload->file_name;
                            $img_cfg['quality'] = 99;
                            $img_cfg['master_dim'] = 'height';
                            $img_cfg['height'] = 50;

                            $resize_error = [];
                            $this->image_lib->initialize($img_cfg);
                            if (!$this->image_lib->resize()) {
                                $resize_error[] = $this->image_lib->display_errors();
                            }
                            $this->image_lib->clear();

                            /********End Thumb*********/

                            /*resize and create thumbnail image*/
                            if ($upload->file_size > 1024) {
                                $img_cfg['image_library'] = 'gd2';
                                $img_cfg['source_image'] = getwdir() . 'uploads/gallery/' . $upload->file_name;
                                $img_cfg['maintain_ratio'] = TRUE;
                                $img_cfg['new_image'] = getwdir() . 'uploads/gallery/' . $upload->file_name;
                                $img_cfg['height'] = 500;
                                $img_cfg['quality'] = 100;
                                $img_cfg['master_dim'] = 'height';

                                $this->image_lib->initialize($img_cfg);
                                if (!$this->image_lib->resize()) {
                                    $resize_error[] = $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                /********End resize*********/
                            }
                        }
                    }

                     if (empty($resize_error)) {
                        $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                    } else {
                        $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                    }
                }
            }else{
                $this->output->set_status_header(402, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'try again later']));
            }
        }
    }

    function update($id){
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);
           

           

            unset($post_data['uploaded']);
            unset($post_data['files']);
            

            if ($this->gallery->update($post_data,$id)) {
                if (!empty($uploaded)) {
                    foreach ($uploaded as $upload) {
                        /*INSERT FILE DATA TO DB*/
                        $file_data['gallery_id'] = $id;
                        $file_data['file_name'] = $upload->file_name;
                        $file_data['url'] = base_url() . 'uploads/gallery/';
                        $file_data['path'] = $upload->file_path;

                        $file_id = $this->gallery_file->insert($file_data);
                        if ($file_id) {
                            /*****Create Thumb Image****/
                            $img_cfg['source_image'] = getwdir() . 'uploads/gallery/' . $upload->file_name;
                            $img_cfg['maintain_ratio'] = TRUE;
                            $img_cfg['new_image'] = getwdir() . 'uploads/gallery/thumb/' . $upload->file_name;
                            $img_cfg['quality'] = 99;
                            $img_cfg['height'] = 50;
                            $img_cfg['master_dim'] = 'height';

                            $this->image_lib->initialize($img_cfg);
                            if (!$this->image_lib->resize()) {
                                $resize_error[] = $this->image_lib->display_errors();
                            }
                            $this->image_lib->clear();

                            /********End Thumb*********/

                            /*resize and create thumbnail image*/
                            if ($upload->file_size > 1024) {
                                $img_cfg['image_library'] = 'gd2';
                                $img_cfg['source_image'] = getwdir() . 'uploads/gallery/' . $upload->file_name;
                                $img_cfg['maintain_ratio'] = TRUE;
                                $img_cfg['new_image'] = getwdir() . 'uploads/gallery/' . $upload->file_name;
                                $img_cfg['height'] = 500;
                                $img_cfg['quality'] = 100;
                                $img_cfg['master_dim'] = 'height';

                                $this->image_lib->initialize($img_cfg);
                                if (!$this->image_lib->resize()) {
                                    $resize_error[] = $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                /********End resize*********/
                            }
                        } else {
                            log_massage('debug', 'update files failed on gallery update');
                            $this->output->set_status_header(500, 'Server Down');
                            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later.']));
                            exit;
                        }
                    }

                }
                if (empty($resize_error)) {
                        $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                    } else {
                        $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                    }
            }else{
                log_massage('debug', 'update failed on gallery');
                $this->output->set_status_header(500, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later.']));
            }
        }
    }

    function delete_image($id)
    {
        $gallery = $this->gallery_file->where('id', $id)->get();
        if ($gallery != false) {
            if (file_exists($gallery->path . $gallery->file_name)) {
                unlink($gallery->path . $gallery->file_name);
            }
            $this->gallery_file->delete($id);
            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Image Delete']));
        }else{
            $this->output->set_status_header(400, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later']));
        }
    }
   

    function upload()
    {
        if (!is_dir('uploads/gallery')) {
            mkdir('./uploads/gallery', 0777, TRUE);
        }

        $config['upload_path'] = getwdir() . '/uploads/gallery';
        $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
        $config['max_size'] = 4096;
        $config['file_name'] = date('YmdHis');

        $this->upload->initialize($config);

        if ($this->upload->do_upload('file')) {
            $this->output->set_content_type('application/json')->set_output(json_encode($this->upload->data()));
        }else{
            $this->output->set_status_header(401, 'File Upload Error');
            $this->output->set_content_type('application/json')->set_output($this->upload->display_errors('',''));
        }
    }

     public function delete($id)
    {
        $gallery = $this->gallery->where('id',$id)->get();
        if ($gallery) {
            $gallery_files = $this->gallery_file->where('gallery_id', $id)->get_all();
            if ($gallery_files) {
                foreach ($gallery_files as $file) {
                    if ($this->gallery_file->delete($file->id)) {
                        if(file_exists($file->path.$file->file_name)) {
                            unlink($file->path . $file->file_name);
                        }
                        $status = 1;
                        } else {
                            $status = 0;
                        }
                }
                if ($status == 1) {
                    if ($this->gallery->delete($id)) {
                        $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Gallery Deleted']));
                    }
                } elseif ($status == 0) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Gallery not deleted but some files are deleted']));
                }
            } 
           else {
                if ($this->gallery->delete($id)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Gallery Deleted']));
                } else {
                    $this->output->set_status_header(500, 'Server Down');
                    $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Delete Error']));
                }
            }
        } else {
            $this->output->set_status_header(500, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }

    public function test()
    {
        getwdir();

    }


}