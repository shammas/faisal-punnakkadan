<?php
/**
 * Testimonial_model.php
 * Date: 17/01/19
 * Time: 02:15 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Testimonial_model extends MY_Model
{
public $table = 'testimonials';
    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}