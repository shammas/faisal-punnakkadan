<?php
/**
 * News_model.php
 * Date: 17/01/19
 * Time: 02:30 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class News_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
        $this->timestamps = TRUE;
        $this->pagination_delimiters = array('<li>','</li>','<li class="active">');
        $this->pagination_arrows = array('&lt; Prev','Next &gt;');
    }

}