<?php
/**
 * Media_model.php
 * Date: 17/01/19
 * Time: 02:05 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Media_model extends MY_Model
{
public $table = 'medias';
    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}