
    <div id="blog_banner" class="mt_light_banner">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12 text-left">
	                <div class="bread_crumbs">
	                    <h4>A Bit Of My Thoughts</h4>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<section id="blog_main_sec">
		<div class="container">
			<div class="row">
            <?php
            if (isset($newss) and $newss != false) {
                foreach($newss as $news) {
                ?>
				<div class="col-md-8">
					<div class="post_img">
						<img src="<?php echo $news->url . $news->file_name;?>" alt="">
					</div>
					<div class="post_title">
						<h3><?php echo $news->title;?></h3>
						<ul class="list-inline list-unstyled">
                            <li>
                                <i class="ion-ios-person-outline"></i>&nbsp;
                                Admin
                            </li>
                            <li>
                                <i class="ion-ios-calendar-outline"></i>&nbsp;
                                 <?php echo date(' d-F Y', strtotime($news->date));?>
                            </li>
                        </ul>
					</div>
					<div class="post_body">
						<p>
							<?php echo $news->description;?>
						</p>
					</div>
				</div>
                <?php 
                    }
                }
                ?>
				<aside class="col-sm-4">
                    <section class="widget widget_recent_entries">
                        <h3 class="blog_heading_border">
                            Recent Posts
                        </h3>
                        <ul>
                            <?php
                            if (isset($news1) and $news1 != false) {
                                foreach($news1 as $news) {
                                ?>
                                <li>
                                    <img src="<?php echo $news->url . $news->file_name;?>" alt="image" />
                                    <h4><a href="#"><?php echo $news->title;?></a></h4>
                                    <p><?php echo date('D, d-m-Y', strtotime($news->date));?>| <span> Comment (2)</span></p>
                                </li>
                                <?php 
                                }
                            }
                            ?>
                        </ul>
                    </section>
				</aside>
			</div>
			<div class="row">
                <div class="col-md-12 text-center">
                    <a href="<?php echo base_url();?>blog-listing" class="view_all_posts">Back to All Posts</a>
                </div>
            </div>
		</div>
	</section>
	<div class="clearfix"></div>
    