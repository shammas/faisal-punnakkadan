<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title>Faisal Punnakkadan</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>images/favicon.png">
    <!-- ================ Style sheets ================ -->
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>css/jquery.fancybox.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>css/animate.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/toastr.min.css">
    <link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>venobox/venobox.css" rel="stylesheet">
</head>
<body>
    <div id="preloader">
        <div id="status"></div>
    </div>
    <div id="home"></div>
    <header id="mt_header">
        <nav>
            <div class="navbar-wrapper" id="navigation">
                <div class="navbar navbar-default navbar-fixed-top reveal-menu" role="navigation">
                    <div class="container nav-container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                                            data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <div class="logo">
                                        <a href="<?php echo base_url();?>index">Faisal Punnakkadan</a>
                                    </div>
                                </div> <!-- .navbar-header -->
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active"><a href="<?php echo base_url();?>index#home">my self, </a></li>
                                        <li><a href="<?php echo base_url();?>index#mt_about_me">who am i, </a></li>
                                        <li><a href="<?php echo base_url();?>index#mt_services">what i do, </a></li>
                                        <li><a href="<?php echo base_url();?>index#mt_testimonial">What People Say, </a></li>
                                        <li><a href="<?php echo base_url();?>index#mt_blog">my thoughts, </a></li>
                                        <li><a href="<?php echo base_url();?>gallery">media, </a></li>
                                        <li><a href="<?php echo base_url();?>index#mt_contact">meet me</a></li>
                                        <li><a href="<?php echo base_url();?>suggestions" target="_blank" style="padding: 0 10px;background: black;border-radius: 3px;color: #fff !important;">Suggestions</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>