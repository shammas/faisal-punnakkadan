<footer id="mt_footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="copyright text-left">
                        <p>&copy; COPYRIGHT. 2018 All Rights Reserved </p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="copyright text-right">
                        <p>Powered By <a href="http://cloudbery.com" target="_blank"><img src="<?php echo base_url();?>images/cloudbery.png" alt="Cloudbery Solutions"></a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Jquery Js -->
    <script src="<?php echo base_url();?>js/jquery-2.1.1.min.js"></script>
    <!-- Bootstrap Js -->
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <!-- Typed Js -->
    <script src="<?php echo base_url();?>js/typed.js"></script>
    <!-- Images Loading Js -->
    <script src="<?php echo base_url();?>js/images.js"></script>
    <!-- WOW Js -->
    <script src="<?php echo base_url();?>js/wow.min.js"></script>
    <!-- Masonary Grid -->
    <script src="<?php echo base_url();?>js/masonry.pkgd.min.js"></script>
    <!-- Navigation scroll Js -->
    <script src="<?php echo base_url();?>js/jquery.nav.js"></script>
    <!-- Owl Carousel Js -->
    <script src="<?php echo base_url();?>js/owl.carousel.min.js"></script>
    <!-- Easing Js -->
    <script src="<?php echo base_url();?>js/jquery.easing.min.js"></script>
    <!-- Toastr Js -->
    <script src="<?php echo base_url();?>js/toastr.min.js"></script>
    <!-- Fancy Box Js -->
    <script src="<?php echo base_url();?>js/jquery.fancybox.pack.js"></script>
    <script src="<?php echo base_url();?>js/waypoint.js"></script>
    <script src="<?php echo base_url();?>js/isotope.min.js"></script>
    <!-- Main Js -->
    <script src="<?php echo base_url();?>js/main.js"></script>
      <script src="<?php echo base_url();?>venobox/venobox.min.js"></script>
</body>
</html>