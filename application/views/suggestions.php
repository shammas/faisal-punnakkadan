
    <section id="mt_contact" style="padding-bottom: 0">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="main-title text-center" style="margin-bottom: 35px;">
                        <div class="main-subtitle-top mb-4">Send now</div>
                        <h2>Do you have any suggestions ?</h2>
                        <div class="main-subtitle-bottom mt-3">Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text.</div>
                    </div>
                </div>
                <!-- <div class="col-md-6">
                    <div class="contact_info">
                        <div class="contact_heading">
                            <span>Get in Touch</span>
                            <h3>Contact info</h3>
                        </div>
                        <div class="email_sec">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="location">
                                        <i class="icon-map"></i>
                                        <h5>Location</h5>
                                        <p>21, Lakewood street,London</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="time">
                                        <i class="icon-alarmclock"></i>
                                        <h5>Available Time</h5>
                                        <p>Monday - Saturday</p>
                                        <p>9:00am - 8:00pm</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="location">
                                        <i class="icon-phone"></i>
                                        <div class="phone">
                                            <h5>Phone</h5>
                                            <p>+92-123456-458</p>
                                            <p>+92-123456-458</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="email">
                                        <i class="icon-envelope"></i>
                                        <h5>Email</h5>
                                        <p><a href="mailto:info@yourdomain.co">info@yourdomain.co</a></p>
                                        <p><a href="mailto:office@yourdomain.co">office@yourdomain.co</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="col-md-6 col-md-offset-3">
                    <div class="contact_form" style="box-shadow: none;">
                        <!-- <h3>Stay in Touch</h3> -->
                        <!-- <form id="contact_form"> -->
                        <?php echo form_open_multipart('suggestions', ['id' => 'contact_form','name' => 'contact_form']); ?>
                            <input type="text" name="name" id="name" placeholder="Your name" required style="border: 1px solid #999;border-radius: 3px;">
                            <input type="text" name="number" id="number" placeholder="Your contact No" required style="border: 1px solid #999;border-radius: 3px;">
                            <!-- <input type="email" name="email" id="email" placeholder="Your email" required> -->
                            <textarea cols="30" rows="5" name="message" id="message" placeholder="Your message" required style="border: 1px solid #999;border-radius: 3px;"></textarea>
                             <div class="text-center"><button class="mt_btn_grey" id="submit">SEND MESSAGE<span
                                    class="mt_load"><span></span></span></button></div>
                           <!--  <div class="text-center"><button class="mt_btn_grey" id="submit-btn">SEND MESSAGE<span
                                    class="mt_load"><span></span></span></button></div> -->
                            <div id="msg"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    