
    <section id="mt_banner">
        <div class="container">
            <div class="row relative">
                <div class="col-md-6 col-sm-6 col-xs-6 static">
                    <div class="banner_caption_text">
                        <h1><span class="light_text_rt">I am</span> <span class="kp_typed" data-text1="Faisal Punnakkadan" data-text2="A Corporate Consultant" data-text3="Managing Director" data-loop="true" data-backdelay="3000"></span> </h1>
                        <p>
                            <strong>FAISAL PUNNAKADAN, </strong>
                            who is the Managing Director of the company, is a highly motivated and enthusiastic business personnel with rich Experienced in Administration, Corporate communications, Financial Analysis and planning, initiating and running Trading production business ,investment Management, Resource Planning and Allocation, Cost and Benefit Analysis. He is strongly backed up with his rich education skill set which includes his degree in commerce and master's degree in Business Administration.
                        </p>
                        <p>
                            <a class="know" href="index##mt_about_me">Yeh! want to know more about me ?</a>
                        </p>                                    
                        <div class="social_icons pull-left">
                            <a href="https://www.facebook.com/faisal.punnakkadan" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="#" title="Behance" target="_blank"><i class="fa fa-behance"></i></a>
                            <a href="#" title="LinkedIn+" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a>
                        </div>
                        <div class="clearfix"></div>
                        <p class="signature"><img src="images/signature.png" alt="Signature"></p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-5 col-md-offset-1 col-xs-offset-1 col-md-offset-1">
                    <div class="banner_caption_img">
                        <img src="images/16.png" alt="">
                    </div>
                </div>
            </div>
        </div>                         
    </section>
    <div id="mt_about_me">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="mt_about_me">
                        <div class="about_heading">
                            <span>Who am I</span>
                            <h3>Hi. I’m Faisal Punnakkadan.</h3>
                        </div>
                        <p>
                            HE​ has developed a highly diversified Organization serving a broad spectrum of various sectors
							of the consultation, Manufacturing, trading and contracting Industry in South India. He meets
							business challenges in innovative ways and responding proactively to our customers problem.</p>

						<p>	His Mission ​ is to foster the spirit of responsibility, our company maintains a high level of quality
							health and safety standards in the operations, cultivate an organisational culture, teamwork and
							excellence in the organisation, that values high performance and innovations and to make
							footprints in various trading, manufacturing, contracting constructing and consultant sectors for
							our habitué.</p>

                        <a href="<?php echo base_url(); ?>assets/faisal-punnakkadan.pdf" class="mt_btn_grey" download>Download Biography</a>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-push-1 col-md-5">
                    <div class="about_img">
                        <img src="images/2.jpg" alt="About Me">
                    </div>
                </div>
            </div>
        </div>
    </div>
     
    <div id="mt_services">
        <div class="container">
            <div class="row ">
               <div class="col-md-8 ">
                    <div class="main-title ">
                        <!-- <div class="main-subtitle-top mb-4">What we Offer</div> -->
                        <h2>Dream Day Is Coming</h2>
                        <div class="main-subtitle-bottom mt-3">​ Not everything in life and business goes exactly as planned. It was a
							shock to the system when failure happens, but we learned it's not the end of the world.
							Everything is a launching point for even greater success.This experience taught us how to
							address failure, adapt, and then recover by reinventing ourselves...
						</div><br>
						<div class="main-subtitle-bottom mt-3">​ Biggest aim is to support the Economically Weaker Section Society development plan. People
							with no stable source of income or sufficient food/water and housing are analyzed,will be trained
							to manage the self employment part and provide them with shelter and earnings.
						</div>
                    </div>
                </div>
                <div class="col-md-4 video-box align-self-baseline aos-init aos-animate" data-aos="zoom-in" data-aos-delay="100">
         <img src="images/16.png" class="img-fluid" alt="">
            <a href="https://www.youtube.com/" class="venobox play-btn mb-4 vbox-item" data-vbtype="video" data-autoplay="true"></a>
          </div>
            </div>
               <div class="row">
                <div class="service_gid">
                <div class="col-md-4">
                    <div class="service-box text-center">
                        <h5 class="mt-3">Contracting</h5>
                        <p class="mt-3 mb-4">Include high-rise and specialty developments in the residential, commercial and retail
							sectors. Have extensive experience in building residences, resorts, hotels, restaurants,
							commercial complexes, educational campuses, hospitals, IT parks, townships and a lot more.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-box text-center">
                        <h5 class="mt-3">Property Consultants</h5>
                        <p class="mt-3 mb-4">In the field of Properties and developers since 2011,dealing with all kinds of properties
							and lands and also avail loans for corporate, to improve operations, drive down costs,
							maximising profitability and provide a safe and productive living and working environment.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-box text-center">
                        <h5 class="mt-3">Facility Management</h5>
                        <p class="mt-3 mb-4">Through his global partners, focus on identifying new techniques which improve quality,
							function and cost to our clients. Skilled at finding unique solutions to the international-specific
							issues & dedicated to creating spaces which are designed, built, maintained and operated
							sustainably.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-box text-center">
                        <h5 class="mt-3">IT Support & Service</h5>
                        <p class="mt-3 mb-4">Delivers you the best-of-breed solutions with powerful combination of technology and
                           comprehensive business applications. Functional analysis and process mapping often take
                           place on request, to enable IT solutions to support business functions efficiently.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-box text-center">
                        <h5 class="mt-3">Export and Import</h5>
                        <p class="mt-3 mb-4">International trade is one of the hot industries of the new millennium. Handles export
							operations for a domestic company that wants to sell its product overseas. We are
							importing machineries from foreign manufacturers and resells the goods, by analyzing all
							the risk.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service-box text-center">
                        <h5 class="mt-3">Products</h5>
                        <p class="mt-3 mb-4">For building residences, resorts, hotels, restaurants, commercial complexes, educational
							campuses, hospitals, IT parks, townships etc. Products which are inexpensive and durable are
							on top priority. We are the C & F Sole Distributor of many main brands of ReadyMix Mortar &
							Build Fast Dry Cement Plaster and TMT. WE manufacture and trade ​ <b>80% OF BUILDING
							MATERIALS.</b></p>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <section id="mt_testimonial">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="main-title text-center">
                        <div class="main-subtitle-top mb-4">TESTIMONIALS</div>
                        <h2>What People Say About</h2>
                        <div class="main-subtitle-bottom mt-3"><!-- Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text. --></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="owl-testimonials" class="owl-carousel owl-theme">
                        <?php
                        if (isset($testimonials) and $testimonials) {
                            foreach ($testimonials as $testimonial) {
                            ?>
                            <div class="item">
                                <div class="testimonial-box">
                                    <img  src="<?php echo $testimonial->url . $testimonial->file_name;?>" alt="" />
                                    <p class="mt-4 mb-5"><?php echo $testimonial->description;?></p>
                                    <h6><?php echo $testimonial->name;?></h6>
                                    <p class="designation_testimonial"><span><?php echo $testimonial->designation;?></span></p>
                                </div>
                            </div>
                            <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="mt_blog">
        <div class="container">
            <div class="row blog_post_sec">
                <div class="col-md-8 col-md-offset-2">
                    <div class="main-title text-center">
                        <div class="main-subtitle-top mb-4">NEWS and webinaris</div>
                        <h2>a bit of my thoughts</h2>
                        <div class="main-subtitle-bottom mt-3"><!-- Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text. --></div>
                    </div>
                </div>
                <?php
                if (isset($newss) and $newss != false) {
                    foreach($newss as $news) {
                    ?>
                    <div class="col-md-4 col-sm-12 grid-item">
                        <div class="blog-post_wrapper">
                            <div class="blog-post-inner_wrapper">
                                <div class="blog-post-image">
                                    <div class="effects clearfix effect-bounce">
                                        <div class="img">
                                            <img src="<?php echo $news->url . $news->file_name;?>" alt="image"
                                                 class="img-responsive center-block post_img"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-detail_container">
                                    <div class="post-content">
                                        <h3 class="post-title entry-title">
                                            <a href="<?php echo base_url('blog-single/' . $news->id. '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $news->title), '-'); ?>">
                                                <?php echo $news->title;?>
                                            </a>
                                        </h3>
                                        <p class="post-excerpt">
                                            <?php echo  word_limiter($news->description, 30, '...');?>
                                        </p>
                                        <p class="view_blog_btn">
                                            <span><?php echo date('d M Y', strtotime($news->date));?></span>
                                            <a href="<?php echo base_url('blog-single/' . $news->id. '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $news->title), '-'); ?>" class="mt_btn_grey">Read more</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                    }
                }
                ?>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="blog-listing" class="view_all_posts">View All Posts</a>
                </div>
            </div>
        </div>
    </section>
    <section id="mt_contact">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="main-title text-center">
                        <div class="main-subtitle-top mb-4">Get In Touch</div>
                        <h2>Any time Just say 'Hi'</h2>
                        <div class="main-subtitle-bottom mt-3"><!-- Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text. --></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact_info">
                        <div class="contact_heading">
                            <span>Get in Touch</span>
                            <h3>Contact info</h3>
                        </div>
                        <div class="email_sec">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="location">
                                        <i class="icon-map"></i>
                                        <h5>Location</h5>
                                        <p>MGS Associates,</p>
                                        <p>No.6/452 F, Opposite Nilambur Teak Museum,</p>
                                        <p>C.N.G Road, Nilambur, Malappuram - 679329.</p>
                                        <p><b>Branches</b> : Cochin/Bangalore/Chennai.</p>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-6">
                                    <div class="time">
                                        <i class="icon-alarmclock"></i>
                                        <h5>Available Time</h5>
                                        <p>Monday - Saturday</p>
                                        <p>9:00am - 8:00pm</p>
                                    </div>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="location">
                                        <i class="icon-phone"></i>
                                        <div class="phone">
                                            <h5>Phone</h5>
                                            <p>+92-123456-458</p>
                                            <p>+92-123456-458</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="email">
                                        <i class="icon-envelope"></i>
                                        <h5>Email</h5>
                                        <p><a href="http://www.mgsassociates.in" target="_blank">md@mgsassociates.in</a></p>
                                        <p><a href="http://info@mgsassociates.in/contact-us.html" target="_blank">info@mgsassociates.in</a></p>
                                        <p><a href="http://www.mgsassociates.in" target="_blank">www.mgsassociates.in</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact_form">
                        <h3>Stay in Touch</h3>
                        <!-- <form id="contact_form"> -->
                        <?php echo form_open_multipart('index', ['id' => 'contact_form','name' => 'contact_form']); ?>
                            <input type="text" name="name" id="name" placeholder="Your name" required>
                            <input type="email" name="email" id="email" placeholder="Your email" required>
                            <textarea cols="30" rows="5" name="message" id="message" placeholder="Your message"
                                      required></textarea>
                            <button class="mt_btn_grey" id="submit">SEND MESSAGE<span
                                    class="mt_load"><span></span></span></button>
                            <!-- <button class="mt_btn_grey" id="submit-btn">SEND MESSAGE<span
                                    class="mt_load"><span></span></span></button> -->
                            <div id="msg"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    