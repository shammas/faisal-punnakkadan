
	<div id="mt_portfolio" class="gallery-section gallery-page">
	    <div class="container">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="main-title text-center">
	                <div class="main-subtitle-top mb-4">My memmories</div>
	                <h2>every moments can tell a story...</h2>
	                <div class="main-subtitle-bottom mt-3">Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text.</div>
	            </div>
	        </div>
		    <div class="grid-wraper clearfix">
		    <?php
                if (isset($medias) and $medias != false) {
                    foreach($medias as $media) {
                    ?>
			        <div class="grid-box float-inline third">
			            <a href="<?php echo $media->url . $media->file_name;?>" class="fancybox">
			                <div class="portfolio-box">
			                    <img  src="<?php echo $media->url . $media->file_name;?>" alt="" />
			                </div>
			            </a>
			        </div>
		   			<?php
                    }
                }
                ?>		        
		    </div>
	    </div>
	</div>
	<div class="clearfix"></div>
	