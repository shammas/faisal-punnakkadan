
	<div id="blog_banner" class="mt_light_banner">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12 text-left">
	                <div class="bread_crumbs">
	                    <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</h4>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<section id="blog_single_main_sec">
		<div class="container">
			<div class="row grid-masonry">
				<!--=== Post Item ===-->
				<?php
                if (isset($newss) and $newss != false) {
                    foreach($newss as $news) {
                    ?>
		            <div class="col-md-4 col-sm-6 grid-item">
		                <div class="blog-post_wrapper">
		                    <div class="blog-post-inner_wrapper">
		                        <div class="blog-post-image">
		                            <div class="effects clearfix effect-bounce">
		                                <div class="img">
		                                    <img src="<?php echo $news->url . $news->file_name;?>" alt="image" class="img-responsive center-block post_img" />
		                                </div>
		                            </div>
		                        </div>
		                        <div class="post-detail_container">
		                            <div class="post-content">
		                                <h3 class="post-title entry-title">
		                                    <a href="<?php echo base_url('blog-single/' . $news->id. '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $news->title), '-'); ?>">
		                                       <?php echo $news->title;?>
		                                    </a>
		                                </h3>
		                                <ul class="list-unstyled list-inline post-metadata">
		                                    <li>
		                                        <i class="ion-ios-stopwatch-outline"></i><?php echo date('D, d-m-Y', strtotime($news->date));?>&nbsp;&nbsp;|
		                                    </li>
		                                </ul>
		                                <p class="post-excerpt">
		                                   <?php echo  word_limiter($news->description, 30, '...');?>
		                                </p>
		                                <a class="more-btn" href="<?php echo base_url('blog-single/' . $news->id. '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $news->title), '-'); ?>">Read more</a>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
	            	<?php 
                    }
                }
                ?>
	            <!--=== Post Item End/===-->
	            
			</div>
			<!--=== Pagination ===-->
			<div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <ul class="pagination text-center">
                        <!-- <li><?php echo $previous_page;?></li> -->
                       	<li><?php echo $all_pages;?></li>
                        <!-- <li><?php echo $next_page;?></li> -->
                        <!-- <li class="next"><a href="#">Next >></a></li> -->
                            <!-- <li class="prev"><a href="#">&lt;&lt; Prev</a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">...</a></li>
                            <li><a href="#">10</a></li>
                            <li class="next"><a href="#">Next >></a></li> -->
                        </ul>
                    </div>
                </div>
           	</div>
		</div>
	</section>
	