/**
 * Created by psybo-03 on 13/07/18.
 */

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename');
    concatCss = require('gulp-concat-css'),
    cleanCSS = require('gulp-clean-css'),
    copydir = require('copy-dir');

var config = {
    publicDir: 'public/',
    adminDir: 'public/assets/dashboard/',
    adminJsDir: 'public/assets/dashboard/js/',
    adminCssDir: 'public/assets/dashboard/css/',
    adminImgDir: 'public/assets/dashboard/img/',
    angularResource: 'resources/assets/js/angular/',
    cssResource: 'resources/assets/css/',
    imgResource: 'resources/assets/img/',
    resource: 'resources/',
    nodeDir: 'node_modules/',
    bowerDir: 'bower_components/'
};

gulp.task('scripts', function () {
    return gulp.src([
        config.angularResource + '*.js',
        config.angularResource + 'controller/*.js'
    ])
        .pipe(concat('angularApp.js'))
        .pipe(gulp.dest(config.adminJsDir))
        .pipe(rename('angularApp.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.adminJsDir));
});

gulp.task('mix', function () {
    return gulp.src([
        config.nodeDir + 'angular/angular.js',
        config.nodeDir + 'angular-route/angular-route.js',
        config.nodeDir + 'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
        config.nodeDir + 'angular-utils-pagination/dirPagination.js',
        config.nodeDir + 'ng-file-upload/dist/ng-file-upload.js',
        config.nodeDir + 'ng-file-upload/dist/ng-file-upload-shim.js',
        config.nodeDir + 'angular-confirm1/js/angular-confirm.js',
        config.nodeDir + 'angular-loading-bar/build/loading-bar.js'
    ])
        .pipe(concat('angular-bootstrap.js'))
        .pipe(gulp.dest(config.adminJsDir))
        .pipe(rename('angular-bootstrap.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.adminJsDir));
});


gulp.task('js', function () {
    return gulp.src([
        config.nodeDir + 'lightbox2/dist/js/lightbox-plus-jquery.js'
    ])
        .pipe(concat('lightbox-plus-jquery.js'))
        .pipe(gulp.dest(config.adminJsDir))
        .pipe(rename('lightbox-plus-jquery.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.adminJsDir));
});

//gulp.task('style', function () {
//    return gulp.src([
//        config.cssResource + '**.css'
//        //config.cssResource + '**.css'
//    ])
//        .pipe(concatCss('app.css'))
//        .pipe(gulp.dest(config.adminCssDir))
//        .pipe(rename('app.min.css'))
//        .pipe(cleanCSS({compatibility: 'ie8'}))
//        .pipe(gulp.dest(config.adminCssDir));
//
//});


/*Copy images to public*/
gulp.task('images', function() {
    return gulp.src([
        config.nodeDir + 'lightbox2/dist/images/**.*'
    ])
        .pipe(gulp.dest(config.adminImgDir));
});

/*Copy css to resources*/
gulp.task('css', function() {
    return gulp.src([
        config.nodeDir + 'lightbox2/dist/css/lightbox.css',
        config.nodeDir + 'angular-confirm1/css/angular-confirm.css',
        config.nodeDir + 'angular-loading-bar/build/loading-bar.css'
    ])
        .pipe(gulp.dest(config.adminCssDir));
});

/*Copy directory to public dir*/
gulp.task('copy', function () {
    copydir.sync(config.resource + 'assets/', config.adminDir);
    //copydir.sync(config.resource + 'assets/font-awesome/', config.adminDir);
    //copydir.sync(config.resource + 'assets/fonts', config.adminDir);
});

gulp.task('install', ['images', 'css', 'js']);
gulp.task('full', ['css', 'js', 'scripts', 'copy', 'mix', 'images']);
gulp.task('default', ['scripts']);